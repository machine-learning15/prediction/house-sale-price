# House Sale Price Prediction - Kaggle Competition
Dataset was collected by Dean De Cock and contains attributes of individual residential properties sold in Ames, Iowa from 2006 to 2010. My goal is to have a prediction score higher than the baseline model, which is a starter model in Kaggle's House Prediction Competition.  The baseline model uses only a few of the existing attributes, and does not do any transformation or feature engineering.

- [Preliminary Preprocessing, Feature Engineering and Results](https://gitlab.com/machine-learning15/prediction/house-sale-price/-/blob/development/ameshousedataset-predictsaleprice-feature-engineering.ipynb)
 
